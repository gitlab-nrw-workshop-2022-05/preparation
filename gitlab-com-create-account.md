# Account auf GitLab.com erstellen

Im Workshop soll gemeinsam mit den anderen Teilnehmern ausprobiert werden wie sich git und GitLab in einer kleinen Gruppe Nutzen lassen. Dafür müssen alle Teilnehmer einen Account auf einem GitLab server haben. Im workshop nutzen wir die Cloud-Version [GitLab.com](https://www.gitlab.com).

## Erster Kontakt

Falls Sie noch keinen Account auf [GitLab.com](https://www.gitlab.com) haben müssten Sie sich zunächst registrieren. Öffenen Sie dazu die Webseite https://gitlab.com/users/sign_up und füllen Sie dort das Registrierungsformular aus oder Nutzen Sie eine der verschiedenen "Social Sign On" möglichkeiten (z.B. Google, GitHub oder Twitter).

Auf der [Hauptseite](README.md#anmelden-bei-gitlabcom) geht es weiter mit Schritt 2.
